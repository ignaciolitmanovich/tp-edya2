module ArrSeq where
import qualified Arr as A
import Par
import Seq

emptyA :: A.Arr a
emptyA = A.empty       

singletonA :: a -> A.Arr a
singletonA x = A.fromList [x]

lengthA :: A.Arr a -> Int 
lengthA x = A.length x

nthA :: A.Arr a -> Int -> a
nthA seq x = (A.subArray x 1 seq) A.! 0

tabulateA :: (Int -> a) -> Int -> A.Arr a
tabulateA f n = A.tabulate f n 

mapA :: (a -> b) -> A.Arr a -> A.Arr b
mapA f seq = A.tabulate (\i -> f (nthA seq i)) n
                            where
                                n = (A.length seq)

filterA :: (a -> Bool) -> A.Arr a -> A.Arr a
filterA f seq 
    | n == 0 = seq
    | otherwise =  joinA (tabulateA (\x -> if (f (nthA seq x)) then singletonA (nthA seq x) else emptyS) n)
        where n = lengthA seq

appendA    :: A.Arr a -> A.Arr a -> A.Arr a
appendA xs ys = A.tabulate f (A.length xs + A.length ys) 
    where
        f n | n <= (A.length xs - 1) = nthA xs n
            | otherwise = nthA ys (n - A.length xs)

takeA :: A.Arr a -> Int -> A.Arr a
takeA seq x = 
    A.subArray 0 x seq 

dropA :: A.Arr a -> Int -> A.Arr a
dropA seq x = 
    A.subArray x (n-x) seq 
        where
            n = A.length seq

showtA :: A.Arr a -> TreeView a (A.Arr a) --11
showtA seq
    | n == 0 = EMPTY
    | n == 1 = ELT (nthA seq 0)
    | otherwise = 
        let half = div n 2
            (l,r) = takeA seq half ||| dropA seq half
        in NODE l r
            where
                n = A.length seq

showlA :: A.Arr a -> ListView a (A.Arr a)
showlA xs 
    | n == 0 = NIL
    | otherwise = 
        let (y, ys) = (nthA xs 0) ||| dropA xs 1
        in CONS y ys
        where
            n = A.length xs

joinA :: A.Arr (A.Arr a) -> A.Arr a
joinA seq = A.flatten seq

reduceA  :: (a -> a -> a) -> a -> A.Arr a -> a
reduceA f e seq = reduceS' f e seq (A.length seq)

reduceS' :: (a -> a -> a) -> a -> A.Arr a -> Int -> a
reduceS' f e seq 2 = f e (f (nthA seq 0) (nthA seq 1))
reduceS' f e seq n = reduceS' f e (sumaPar f seq) n'
        where n'= if even n then (div n 2) else ((div n 2) + 1)

sumaPar :: (a->a->a) -> A.Arr a -> A.Arr a
sumaPar f seq
    |(A.length seq) <= 1 = seq
    |otherwise = 
        let (xy,seq') = f x y ||| sumaPar f (A.subArray 2 (n-2) seq)
        in appendA (singletonA xy) seq'
            where 
                n = A.length seq
                x = nthA seq 0
                y = nthA seq 1

fun :: (a -> a -> a) -> Int -> A.Arr a -> A.Arr a -> a
fun f x seq1 seq2 
            | even x = nthA seq2 (div x 2) 
            | otherwise = f (nthA seq2 (div x 2)) (nthA seq1 (x-1)) 

expansion :: (a -> a -> a) -> A.Arr a -> (A.Arr a , a) -> (A.Arr a, a) 
expansion f seq1 (seq2,a) = (tabulateA (\x-> fun f x seq1 seq2) (lengthA seq1), a)

scanA :: (a -> a -> a) -> a -> A.Arr a-> (A.Arr a, a)
scanA f b seq 
    | lengthA seq == 1 = (singletonA b) ||| f b (nthA (sumaPar f seq) 0)
    | otherwise = 
        let tupla = scanA f b (sumaPar f seq) 
        in expansion f seq tupla

fromListA :: [a] -> A.Arr a
fromListA xs = A.fromList xs

instance Seq A.Arr where
   emptyS = emptyA
   singletonS x = singletonA x 
   lengthS seq = lengthA seq
   nthS seq = nthA seq  
   tabulateS f n = tabulateA f n
   mapS f seq = mapA f seq  
   filterS f seq = filterA f seq
   appendS seq1 seq2 = appendA seq1 seq2
   takeS seq n = takeA seq n
   dropS seq n = dropA seq n 
   showtS seq = showtA seq 
   showlS seq = showlA seq
   joinS seq = joinA seq  
   reduceS f e seq = reduceA f e seq
   scanS f e seq = scanA f e seq
   fromList seq = fromListA seq
