{- Implementación del TAD secuencia -}

module Seq where

class Seq s where
   emptyS     :: s a -- 1
   singletonS :: a -> s a
   lengthS    :: s a -> Int -- 3
   nthS       :: s a -> Int -> a 
   tabulateS  :: (Int -> a) -> Int -> s a --5
   mapS       :: (a -> b) -> s a -> s b 
   filterS    :: (a -> Bool) -> s a -> s a  -- 7
   appendS    :: s a -> s a -> s a
   takeS      :: s a -> Int -> s a -- 9
   dropS      :: s a -> Int -> s a
   showtS     :: s a -> TreeView a (s a) --11
   showlS     :: s a -> ListView a (s a)
   joinS      :: s (s a) -> s a -- 13
   reduceS    :: (a -> a -> a) -> a -> s a -> a
   scanS      :: (a -> a -> a) -> a -> s a -> (s a, a) -- 15
   fromList   :: [a] -> s a

data TreeView a t = EMPTY | ELT a | NODE t t deriving Show
data ListView a t = NIL | CONS a t deriving Show
