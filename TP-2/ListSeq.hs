module ListSeq where
import Par
import Seq

is_emptyL :: [a] -> Bool
is_emptyL [] = True
is_emptyL _ = False

emptyL :: [a]
emptyL = []

singletonL :: a -> [a]
singletonL x = [x]

lengthL :: [a] -> Int
lengthL xs = lengthS' 0 xs
    where lengthS' a [] = a
          lengthS' a (_:xs) = lengthS' (a+1) xs

nthL :: [a] -> Int -> a
nthL (x :xs) 0 = x
nthL (x :xs) n = nthL xs (n-1)

tabulateL  :: (Int -> a) -> Int -> [a]
tabulateL f n = tab 0 (n-1)
    where
        tab min max | min > max = emptyL
                    | otherwise = 
                        let m = div (max + min) 2
                            ((l,r),fx) = (tab min (m-1) ||| tab (m+1) max) ||| [f m] 
                        in l ++ fx ++ r

mapL :: (a -> b) ->[a] -> [b]
mapL _ [] = []
mapL f (x:xs) = let (y,ys) = f x ||| mapL f xs
                in y : ys

filterL :: (a -> Bool) -> [a] -> [a]
filterL f xs = [x | x <- xs, f x]

appendL :: [a] -> [a] -> [a]
appendL x [] = x
appendL [] y = y
appendL x y = x ++ y

takeL :: [a] -> Int -> [a]
takeL [] _ = []
takeL x 0 = []
takeL (x:xs) n = x : (takeL xs (n-1)) 

dropL :: [a] -> Int -> [a]
dropL [] _ = []
dropL x 0 = x
dropL (x:xs) n = dropL xs (n-1)

showtL :: [a] -> TreeView a ([a])
showtL [] = EMPTY
showtL [x] = ELT x
showtL xs = 
    let half = div n 2
        (l,r) = take half xs ||| drop half xs
    in NODE l r
    where
        n = lengthL xs

showlL :: [a] -> ListView a [a]
showlL [] = NIL
showlL (x:xs) = CONS x  xs

joinL :: [[a]] -> [a]
joinL [] = []
joinL (x:xs) = joinS' x xs
    where joinS' x [] = x
          joinS' a (x:xs) = joinS' (a ++ x) xs

sumaPar :: (a -> a -> a) -> [a] -> [a]
sumaPar f (x:y:xs) =
    let (xy, xs') = f x y ||| sumaPar f xs
    in xy : xs'

sumaPar _ xs = xs

reduceL :: (a->a->a) -> a -> [a] -> a
reduceL _ b [] = b
reduceL f b (x:xs)
    |is_emptyL xs = f b x
    |otherwise = reduceL f b (sumaPar f (x:xs))

fun :: (a -> a -> a) -> Int -> [a] -> [a] -> a
fun f x seq1 seq2 
            | even x = nthS seq2 (div x 2)
            | otherwise = f (nthS seq2 (div x 2)) (nthS seq1 (x-1)) 

expansion :: (a -> a -> a) -> [a] -> ([a], a) -> ([a], a)
expansion f seq1 (seq2,a) = (tabulateL (\x-> fun f x seq1 seq2) (lengthL seq1), a)

scanL :: (a -> a -> a) -> a -> [a] -> ([a], a)
scanL f b seq = agregarAlprincipio b (scanL' f b seq n) 
        where
            n = lengthL seq
            scanL' f b seq n
                | n == 1 =  (emptyS) ||| f b (head (sumaPar f seq))
                | otherwise = 
                    let (seq2,a) = scanL' f b redu (if even n then (div n 2) else (div n 2) + 1) 
                    in ((parimpar f b b seq seq2 n), a)
                        where redu = (sumaPar f seq)

-- W_scanLf n = W_scanL ([n/2]techo) + W_sumaPar(n) + W_parimpar([n/2]techo)

par f e prev seq seq2 n | n == 1 = []
                        | otherwise = (new :(impar f e new (tail seq) (tail seq2) (n-1)))
                          where new = head seq2
                          
impar f e prev seq seq2 n | n == 1 = []
                          | n == 2 = [new]
                          | otherwise = (new :(par f e new (tail seq) seq2 (n-1)))
                            where new = f prev (head seq)

parimpar f e prev seq seq2 n
    | n == 1 = []
    | n == 2 = [newfst]
    | otherwise = newfst : newsnd : (parimpar f e newsnd (tail (tail seq)) (tail seq2) (n-2)) 
        where
            newsnd = head seq2
            newfst = f prev (head seq)

agregarAlprincipio :: a -> ([a], a) -> ([a], a)
agregarAlprincipio x (lista, elemento) = (x : lista, elemento)

fromListL :: [a] -> [a]
fromListL seq = seq

instance Seq [] where
   emptyS = emptyL
   singletonS x = singletonL x 
   lengthS seq = lengthL seq
   nthS seq = nthL seq  
   tabulateS f n = tabulateL f n
   mapS f seq = mapL f seq  
   filterS f seq = filterL f seq
   appendS seq1 seq2 = appendL seq1 seq2
   takeS seq n = takeL seq n
   dropS seq n = dropL seq n 
   showtS seq = showtL seq 
   showlS seq = showlL seq
   joinS seq = joinL seq  
   reduceS f e seq = reduceL f e seq
   scanS f e seq = scanL f e seq
   fromList seq = fromListL seq