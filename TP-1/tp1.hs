{-
    Trabajo Práctico N°1
    Estructuras de Datos y Algoritmos 2
    Integrantes: Ignacio Litmanovich.
                 Facundo Pereyra.
-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

data TTree k v = Node k (Maybe v) (TTree k v) (TTree k v) (TTree k v)
                 | Leaf k v
                 | E
                 deriving Show

--1) search: Devuelve el valor asociado a una clave.
search :: Ord k => [k] -> TTree k v -> Maybe v
search  _ E = Nothing
search (x:xs) (Leaf k v) = if (x == k) && (xs == []) then Just v else Nothing
search (x:xs) (Node k v l m r)  | x < k  = search (x:xs) l
                                            | x == k = if (xs == []) then v else search xs m
                                            | x > k  = search (x:xs) r

--2) insert: Agrega una clave asocida a un valor en un árbol. Si la clave ya está en el árbol, actualiza su valor.
insert :: Ord k => [k] -> v -> TTree k v -> TTree k v
insert (x:xs) value E = if xs == [] then (Leaf x value) else (Node x Nothing E (insert xs value E) E)
insert (x:xs) value (Leaf k v) = if xs == [] then if x == k then (Leaf k value) else insert [x] value (Node k (Just v) E E E) else insert (x:xs) value (Node k (Just v) E E E)
insert (x:xs) value (Node k v l m r) | x < k   =  (Node k v (insert (x:xs) value l) m r)
                                     | x > k   =  (Node k v l m (insert (x:xs) value r))
                                     | x == k  =  if xs == [] then (Node k (Just value) l m r) else (Node k v l (insert xs value m) r)

--3) delete: Elimina una clave y el valor asociada a ésta en un árbol.
corrigeHoja :: TTree k v -> TTree k v
corrigeHoja (Node k v E E E) = (Leaf k (maybetoint v)) where
                             maybetoint (Just v) = v
corrigeHoja arbol = arbol

esIncorrecto :: TTree k v -> Bool
esIncorrecto (Node k Nothing E E E) = True
esIncorrecto _ = False

isEmpty :: TTree k v -> Bool
isEmpty E = True
isEmpty _ = False

reacomodar :: Ord k => TTree k v -> TTree k v -> TTree k v
reacomodar E arbol = arbol
reacomodar arbol E = arbol
reacomodar (Leaf k1 v1) (Leaf k2 v2) = (Node k1 (Just v1) E E (Leaf k2 v2))
reacomodar (Node k1 v1 l1 m1 r1) (Leaf k v) = if (k < k1) then (Node k (Just v) E E (Node k1 v1 l1 m1 r1)) else (Node k (Just v) (Node k1 v1 l1 m1 r1) E E)
reacomodar (Leaf k v) (Node k1 v1 l1 m1 r1) = if (k < k1) then (Node k (Just v) E E (Node k1 v1 l1 m1 r1)) else (Node k (Just v) (Node k1 v1 l1 m1 r1) E E)
reacomodar (Node k1 v1 l1 m1 r1) (Node k2 v2 l2 m2 r2)  | isEmpty r1 = (Node k1 v1 l1 m1 (Node k2 v2 l2 m2 r2))
                                                        | otherwise = (Node k1 v1 l1 m1 (reacomodar r1 (Node k2 v2 l2 m2 r2)))

delete :: Ord k => [k] -> TTree k v -> TTree k v
delete _ E = E
delete (x:xs) (Leaf k v) = if (xs == []) && (x == k) then E else (Leaf k v)
delete (x:xs) (Node k v l m r) | x < k   =  (Node k v (delete (x:xs) l) m r)
                                | x > k   =  (Node k v l m (delete (x:xs) r))
                                | x == k  =  if xs == [] then (if not (isEmpty m) then (Node k Nothing l m r) else reacomodar l r)
                                else let arbol = (Node k v l (delete xs m) r) in
                                    if esIncorrecto arbol then E else corrigeHoja arbol                        

--4) keys: Dado un árbol devuelve una lista ordenada con las claves del mismo.
isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing _ = False

keys :: TTree k v -> [[k]]
keys E = []
keys (Leaf k v) = [[k]]
keys (Node k v l m r) = if isNothing v then keys l ++ map (k:) (keys m) ++ keys r
                                       else keys l ++ map (k:) (keys m) ++ [[k]] ++ keys r

--5) Instancia de la clase Dic para el tipo de datos TTree k v
empty :: TTree k v
empty = E 

class Dic k v d | d -> k v where
    vacio :: d
    insertar :: Ord k => k -> v -> d -> d
    buscar :: Ord k => k -> d -> Maybe v
    eliminar :: Ord k => k -> d -> d
    claves :: Ord k => d -> [k]

instance Ord k => Dic [k] v (TTree k v) where
    vacio = empty
    insertar k v d = insert k v d
    buscar k d = search k d
    eliminar k d = delete k d
    claves d = keys d